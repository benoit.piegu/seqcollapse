//! Compute Jaccard similarity (and distance) between each pair of banks of sequences
//!
//! The struct [JaccardIndex] store results of each comparison and can return
//! Jaccard similarity and Jaccard distance.
//! 
//! Three functions ([cmp_count], [cmp_count_wo_singleton], [cmp_fold]) allow
//! you to compare the numbers of a sequence between two banks. Theses functions
//! return a enum [SetCmp].
use std::fmt;

use funty::Integral;
use itertools::Itertools;

use crate::seqcollapse::BumpVec;


/// a struct to record a function to compare a member of a two different set
/// and his name
pub struct SetCmpFunc<T: Integral> {
    pub name: String,
    pub setcmp_fn: fn(T, T) -> SetCmp
}

/// Enum to describe whether an element is common or specific to set 0 or specific to set 1
#[derive(Debug, Copy, Clone)]
pub enum SetCmp {
    /// specific set 0
    M01,
    /// specific set 1
    M10,
    /// common
    M11,
    /// not present in set 0 and set 1
    None,
}

impl fmt::Display for SetCmp {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            SetCmp::M01 =>  write!(f, "01"),
            SetCmp::M10 =>  write!(f, "10"),
            SetCmp::M11 =>  write!(f, "11"),
            SetCmp::None => write!(f, "--"),

        }
    }
}

/// Function to compare an element in two different sets
/// This is a element of the two set if the `max((v0,v1))/min((v0,v1)) < FOLD_THRESHOLD`
pub fn cmp_fold<T: Integral>(v0: T, v1: T) -> SetCmp {
    // eprintln!("cmp_fold");
    static FOLD_THRESHOLD: f32 = 100.0;
    if v0 == T::ZERO {
        if v1 == T::ZERO {
            SetCmp::None
        } else {
            SetCmp::M10
        }
    } else if v1 == T::ZERO {
        SetCmp::M01
    } else {
        match (v0, v1) {
            (v0, v1) if v0 > v1 && v0.as_f32() / v1.as_f32() >= FOLD_THRESHOLD => SetCmp::M01,
            (v0, v1) if v1 > v0 && v1.as_f32() / v0.as_f32() >= FOLD_THRESHOLD => SetCmp::M10,
            _ => SetCmp::M11,
        }
    }
}

/// Function to compare a element in two different sets.
/// This is a member of the two set (M11)  if `v0!=0 && v1!=0`
pub fn cmp_count<T: Integral>(v0: T, v1: T) -> SetCmp {
    // eprintln!("cmp_count");
    match (v0, v1) {
        (v0, v1) if v0 > T::ZERO && v1 == T::ZERO => SetCmp::M01,
        (v0, v1) if v0 == T::ZERO && v1 > T::ZERO => SetCmp::M10,
        (v0, v1) if v0 > T::ZERO && v1 > T::ZERO => SetCmp::M11,
        _ => SetCmp::None,
    }
}

/// Function to compare a element in two different sets.
/// This is a member of the two set (M11) if `v0>1 && v1>1`
/// This is a member of set 01 (or 10) if `v01>1` (or `v10>1`)
pub fn cmp_count_wo_singleton<T: Integral>(v0: T, v1: T) -> SetCmp {
    // eprintln!("cmp_count_wo_singleton");
    match (v0, v1) {
        (v0, v1) if v0 <= T::ONE && v1 <= T::ONE => SetCmp::None,
        (v0, v1) if v0 > T::ONE && v1 == T::ZERO => SetCmp::M01,
        (v0, v1) if v0 == T::ZERO && v1 > T::ONE => SetCmp::M10,
        (v0, v1) if v0 > T::ONE && v1 > T::ONE => SetCmp::M11,
        _ => SetCmp::None,
    }
}


/// A struct containing count of common and specific members for  all combinations of pairs of sets
#[derive(Clone, Debug)]
pub struct JaccardIndex {
    /// The number of sets to compare
    pub n_set: usize,
    /// A vector containing all combinations of pairs of sets, represented by tuples of indices on the banks.
    pub pairs: Vec<(usize, usize)>,
    /// A vector containing the number of members specific to set 0 for each pair of sets in `pairs`.
    pub v_n_spe0: Vec<usize>,
    /// A vector containing the number of members specific to set 1 for each pair of sets in `pairs`.
    pub v_n_spe1: Vec<usize>,
    /// A vector containing the number of common members between sets 0 and 1 for each combination of pair of sets in `pairs`.
    pub v_n_common: Vec<usize>,
}

impl JaccardIndex {

    /// Create JaccardIndex structure for comparing n_set
    pub fn new(n_set: usize) -> Self {
        let pairs = (0..n_set)
            .combinations(2)
            .map(|v| (v[0], v[1]))
            .collect::<Vec<(usize, usize)>>();
        let n_pairs = pairs.len();
        JaccardIndex {
            n_set,
            pairs,
            v_n_spe0: vec![0usize; n_pairs],
            v_n_spe1: vec![0usize; n_pairs],
            v_n_common: vec![0usize; n_pairs],
        }
    }

    /// Count specific and common member for each pair of sets in `pairs`.
    ///
    ///
    /// The iterator `values_by_set_iter` should return a vector of length `n_set`.
    /// and should contain the count of the studied member for each set.
    ///
    /// The provided function, `fn_cmp`, should be able to compare the counts
    ///  of the member between two sets to determine if it is specific or
    /// common, and return a [SetCmp].
    ///
    /// See functions:
    ///
    /// * [cmp_count]
    /// * [cmp_count_wo_singleton]
    /// * [cmp_fold]
    pub fn count_from_iter<'b,'a, I, T>(&mut self, values_by_set_iter: I, fn_cmp: fn(T,T) -> SetCmp)
    where
        T: Integral,
        I: IntoIterator<Item: AsRef<BumpVec<'a, T>>> + 'a,
    {
        //- eprintln!("# function cmp: {:?} ({})", fn_cmp, crate::type_of(&fn_cmp));
        values_by_set_iter.into_iter().for_each(|vec| {
            let v_count = vec.as_ref();
            //- let mut v_res = Vec::<SetCmp>::new();
            self.pairs.iter().enumerate().for_each(|(ipair, (i0, i1))| {
                //- let res = fn_cmp(v_count[*i0], v_count[*i1]);
                //- v_res.push(res);
                match fn_cmp(v_count[*i0], v_count[*i1]) {
                    SetCmp::M01 => self.v_n_spe0[ipair] += 1,
                    SetCmp::M10 => self.v_n_spe1[ipair] += 1,
                    SetCmp::M11 => self.v_n_common[ipair] += 1,
                    _ => (),
                }
            });
            //- let s_count = v_count.iter().map(|x| x.to_string()).collect::<Vec<_>>().join(",");
            //- let s_res = v_res.iter().map(|x| x.to_string()).collect::<Vec<_>>().join(",");
            //- eprintln!("{} => {}", s_count, s_res);
        });
        // eprintln!("#ij={:?}", self);
    }


    /// Call [JaccardIndex::count_from_iter] with the default comparison function
    /// [cmp_count]
    pub fn count_from_iter_default<'a, I, T>(&mut self, values_by_set_iter: I)
    where
        T: Integral,
        I: IntoIterator<Item: AsRef<BumpVec<'a, T>>> + 'a,
    {
        self.count_from_iter(values_by_set_iter, cmp_count);
    }

    /// Returns a vector of tuples containing the indices of each set pair being compared
    /// and the Jaccard distance between them
    #[cfg_attr(doc, katexit::katexit)]
    ///
    /// Jaccard Distance
    /// ------------------
    ///
    /// $$
    /// J_\\delta = 1 - \\frac{|U \\cap V|}{|U \\cup V|}
    /// $$
    pub fn distances(&self) -> Vec<((usize, usize), f32)> {
        // distances = 1 - similarity
        self.similarity()
            .iter()
            .map(|(indices, similarity)| (*indices, 1.0 - similarity))
            .collect()
        /*
        self.pairs
            .iter()
            .enumerate()
            .map(|(ipair, (_i0, _i1))| {
                (
                    self.pairs[ipair],
                    (self.v_n_spe0[ipair] as f32 + self.v_n_spe1[ipair] as f32)
                        / (self.v_n_spe0[ipair] as f32
                            + self.v_n_spe1[ipair] as f32
                            + self.v_n_common[ipair] as f32),
                )
            })
            .collect::<Vec<((usize, usize), f32)>>()
        */
    }

    /// Returns a vector of tuples containing the indices of each set pair being compared
    ///  and the Jaccard similarity coefficient between them
    #[cfg_attr(doc, katexit::katexit)]
    ///
    /// Jaccard Similarity
    /// ------------------
    ///
    /// $$
    /// Jaccard(U,V) = \\frac{|U \\cap V|}{|U \\cup V|} = \\frac{|U \\cap V|}{|U| + |V| - |U \\cap V|}
    /// $$
    pub fn similarity(&self) -> Vec<((usize, usize), f32)> {
        // eprintln!("# {:?}", &self);
        self.pairs
            .iter()
            .enumerate()
            .map(|(ipair, (_i0, _i1))| {
                (
                    self.pairs[ipair],
                    (self.v_n_common[ipair] as f32)
                        / (self.v_n_spe0[ipair] as f32
                            + self.v_n_spe1[ipair] as f32
                            + self.v_n_common[ipair] as f32),
                )
            })
            .collect::<Vec<((usize, usize), f32)>>()
    }
}
