//! Manage configuration of collapser
//!
//! [SeqCollapseConf] is a struct containing all the necessary configuration
//! to do sequence collapse via the [crate::seqcollapse::SeqCollapse] trait
//!
//! [SeqCollapseConfBuilder] is a builder facilitating the creation of a [SeqCollapseConf]
//!
//! ```rust
//! use bumpalo::Bump;
//! use seqcollapse::seqcollapseconf::{SeqCollapseConf,SeqCollapseConfBuilder};
//! use std::error::Error;
//!
//! fn main() -> Result<(), Box<dyn Error>> {
//!     // create the configuration
//!     let conf: SeqCollapseConf = SeqCollapseConfBuilder::default()
//!         .with_add(true)
//!         .with_ignore_case(false)
//!         .with_revcomp(true)
//!         .fastx_files(vec!["sequences.fa".into()])
//!         .factor_norm_freq(1e6)
//!         .build()?;
//!
//!     // init the collapser with an arena
//!     let bump = Bump::with_capacity(1024*1024*8);
//!     let mut collapser: seqcollapse::seqcollapse::BoxedSeqCollapse<'_, ahash::AHasher, u32> =
//!         conf.init_seqcollapse(&bump);
//!     Ok(())
//! }
//! ```
//!
use std::{
    fmt::{self, Display},
    hash::{BuildHasher, BuildHasherDefault, Hasher},
    option::Option,
};

use anyhow::anyhow;
use derive_builder::Builder;
use itertools::Itertools;
use regex::Regex;
use serde::Serialize;

use crate::{get_no_unique, seqcollapse::BoxedSeqCollapse, SeqCount, StatLevel};

/// A struct for storing infput files and options of collapser.
#[derive(Builder, Clone, Debug, Serialize)]
#[builder(default)]
pub struct SeqCollapseConf {
    /// Name and version
    // #[builder(default = "\"seqcollapse\".to_string()")]
    pub name_and_v: String,
    /// Stat level ([crate::StatLevel])
    pub stat_level: StatLevel,
    /// If True, take into account counts detected in sequence id
    pub with_add: bool,
    /// If True, ignore case when comparing sequence
    pub with_ignore_case: bool,
    /// If True, rev comp sequences are also compared
    pub with_revcomp: bool,
    /// List of fastx files to read
    pub fastx_files: Vec<String>,
    /// A regexp to extract a tag from file names
    pub fastxtag_regex: Option<String>,
    /// Tags extracted from file names
    #[builder(setter(skip))]
    pub fastx_tag: Vec<String>,
    /// A factor to calculate standardized sequence counts per file
    pub factor_norm_freq: f32,
}

impl SeqCollapseConf {

    // Creates an a new SecollapseConf.
    pub fn new(
        name_and_v: String,
        stat_level: StatLevel,
        with_add: bool,
        with_ignore_case: bool,
        with_revcomp: bool,
        fastx_files: &[&str],
        fastx_regex: Option<String>,
    ) -> SeqCollapseConf {
        // fastx_files.iter().for_each(|f| self.fastx_files.push(f.to_string()));
        SeqCollapseConf {
            name_and_v,
            stat_level,
            with_add,
            with_ignore_case,
            with_revcomp,
            fastx_files: fastx_files.iter().map(|f| f.to_string()).sorted().collect(),
            fastxtag_regex: fastx_regex,
            fastx_tag: Vec::new(),
            factor_norm_freq: 1.0,
        }
    }

    pub fn _new_for_input_buffer() -> SeqCollapseConf {
        SeqCollapseConf {
            name_and_v: "seqcollapse".into(),
            stat_level: StatLevel::None,
            with_add: false,
            with_ignore_case: false,
            with_revcomp: false,
            fastx_files: vec!["".into()],
            fastx_tag: Vec::new(),
            fastxtag_regex: None,
            factor_norm_freq: 1.0,
        }
    }

    /// returns the value of the with_add option
    pub fn get_with_add(&self) -> bool {
        self.with_add
    }

    /// returns the value of the with_revcomp option
    pub fn get_with_revcomp(&self) -> bool {
        self.with_revcomp
    }

    /// returns the list of input fastx files
    pub fn get_fastx_files(&self) -> &Vec<String> {
        &self.fastx_files
    }

    /// returns the number of banks (fastqx file)
    pub fn nbanks(&self) -> usize {
        self.fastx_files.len()
    }

    /// returns the value of factor_norm_freq
    pub fn get_factor_norm_freq(&self) -> f32 {
        self.factor_norm_freq
    }

    /// initalize and return a [BoxedSeqCollapse]
    pub fn init_seqcollapse<H, T>(self, bump: &bumpalo::Bump) -> BoxedSeqCollapse<H, T>
    where
        T: SeqCount,
        // usize: From<T>,
        H: Hasher,
        BuildHasherDefault<H>: BuildHasher,
    {
        BoxedSeqCollapse::new(self, bump)
    }

    /// initalize with capacity and return a [BoxedSeqCollapse]
    pub fn init_seqcollapse_with_capacity<H, T>(self, bump: &bumpalo::Bump, capacity: usize) -> BoxedSeqCollapse<H, T>
    where
        T: SeqCount,
        // usize: From<T>,
        H: Hasher,
        BuildHasherDefault<H>: BuildHasher,
    {
        BoxedSeqCollapse::with_capacity(self, bump, capacity)
    }

}

impl Default for SeqCollapseConf {
    fn default() -> SeqCollapseConf {
        SeqCollapseConf {
            name_and_v: "seqcollapse".into(),
            stat_level: StatLevel::None,
            with_add: false,
            with_ignore_case: false,
            with_revcomp: false,
            fastx_files: Vec::new(),
            fastx_tag: Vec::new(),
            fastxtag_regex: None,
            factor_norm_freq: 1.0,
        }
    }
}

impl Display for SeqCollapseConf {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        writeln!(f, "# {} -- stat:{}, with_add: {}, with_ignore_case: {}, with_revcomp: {}, fastxtag_regx: {:?}",
            &self.name_and_v, self.stat_level, self.with_add, self.with_ignore_case, self.with_revcomp, self.fastxtag_regex,
        )?;
        let _ = writeln!(f, "# Input: ");
        let res = self
            .fastx_files
            .iter()
            .zip(self.fastx_tag.iter())
            .try_for_each(|(file, tag)| {
                writeln!(
                    f,
                    "#        {tag}\t{}",
                    if self.fastxtag_regex.is_none() {
                        ""
                    } else {
                        file
                    }
                )
            });
        res
    }
}

type VResString = Vec<Result<String, String>>;
#[allow(clippy::needless_return)]
impl SeqCollapseConf {
    /// Apply [SeqCollapseConf::fastxtag_regex] on [SeqCollapseConf::fastx_files] and verifies that
    ///
    /// * each file produces a tag and
    /// * that each tag is unique
    ///
    /// If not, returns an error.
    ///
    pub fn validate_tags(&mut self) -> Result<(), anyhow::Error> {
        if let Some(regex_str) = &self.fastxtag_regex {
            let re = Regex::new(regex_str)?;
            let (v_tags, v_err): (VResString, VResString) = self
                .fastx_files
                .iter()
                .map(|f| {
                    // search for regexp
                    let res = re
                        // and capture
                        .captures(f)
                        // get only the first capture!
                        .map(|c| {
                            let e = c.get(1).unwrap();
                            // eprintln!("capture={:?} => {}", e, e.as_str());
                            e.as_str()
                        })
                        .map(String::from)
                        .ok_or(format!("  <{}>: no capture with regex", f));
                    res
                })
                // partition into vec of Ok() and vec of Err()
                .partition(Result::is_ok);
            // there is some errors
            if !v_err.is_empty() {
                let err = v_err
                    .into_iter()
                    .map(|v| v.unwrap_err())
                    .collect::<Vec<String>>()
                    .join("\n");
                return Err(anyhow!(format!(
                    "Tag can't be defined for some files (no capture from): \n{}",
                    err
                )));
            }
            // eprintln!(" v_tags={:?}", v_tags);
            self.fastx_tag = v_tags
                .into_iter()
                .collect::<Result<Vec<String>, String>>()
                .unwrap();
            let v_no_unique = get_no_unique(&self.fastx_tag);
            // if there is some non unique tag:
            if let Some(v_no_unique) = v_no_unique {
                let err = v_no_unique
                    .iter()
                    .map(|v| {
                        let s = v
                            .iter()
                            .map(|i| (self.fastx_files[*i]).clone())
                            .collect::<Vec<String>>()
                            .join(", ");
                        (s, self.fastx_tag[v[0]].clone())
                    })
                    .map(|(s, tag)| format!("Files {s} have the same tag {tag}!\n"))
                    .collect::<Vec<String>>()
                    .join("\n");
                return Err(anyhow!("err={}", err));
            } else {
                return Ok(());
            }
        } else {
            // eprintln!(" direct: self.fastx_files={:?}", self.fastx_files);
            self.fastx_tag = self.fastx_files.iter().map(|f| f.to_string()).collect();
            Ok(())
        }
    }
}
