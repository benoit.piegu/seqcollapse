//! # Manages outputs that can be either a file, STDOUT or STDERR
//!
//! cf [`io::stdout`] and [`io::stderr`]
//!
//!
//! All output created are buffered. The default size is [OUT_DEFAULT_BUFSIZE]
//!
//! An enum, [OutType], manage the alternative between file, STDOUT and STDERR.
//! A struct, [Out] allow to create an buffered output ([BufWriter]) from an [OutType]
//!
//! * [Out::create()]: create an Out from an [OutType]
//! * [Out::with_capacity()]: idem with specification of buffer capacity
//! * [Out::create_file_or_stdout()]: create an Out from a file path (if `Some(filepath)`) or STDOUT (if `None`)
//! * [Out::create_file_or_stderr()]: idem with STDERR
//!
//!
//! ```
//! use std::{error::Error, io::Write};
//! use seqcollapse::out::Out;
//! use seqcollapse::out::OutType;
//!
//! fn main() -> Result<(), anyhow::Error> {
//!     let file_path = "tmp.txt";
//!     {
//!         // create an output to Stderr
//!         let mut out  = Out::create::<String>(OutType::Stderr)?;
//!         writeln!(out, "# start out");
//!
//!         // an output to a file
//!         let mut out = Out::create(OutType::Path(file_path))?;
//!         writeln!(out, "# start out");
//!
//!         // create an output to a file
//!         let flog: String = "out.log".into();
//!         let mut log = Out::create_file_or_stderr(Some(flog), None)?;
//!
//!         writeln!(log, "# start log");
//!     }
//!     std::fs::remove_file(file_path);
//!     Ok(())
//! }
//!
//! ```
//!
//! Example with use of Clap:
//!
//! ```no_run
//! use clap::{arg, command};
//! use std::{error::Error, io::Write};
//! use seqcollapse::out::Out;
//!
//! fn main() -> Result<(), anyhow::Error> {
//!     let app = command!()
//!         .arg(arg!(-o --out <OUT_FILE> "output file")
//!             .value_parser(clap::value_parser!(String))
//!             .action(clap::ArgAction::Set)
//!             .required(false),
//!         );
//!     let arguments = &app.get_matches();
//!     let mut out = Out::create_file_or_stdout(arguments.get_one::<&str>("out"), None)?;
//!     writeln!(out, "# start out");
//!     Ok(())
//! }
//! ```
//!
use std::{
    fmt,
    fs::File,
    io::{self, BufWriter, Write},
    path::PathBuf,
};

/// The default size of buffer used in out
pub const OUT_DEFAULT_BUFSIZE: usize = 1024 * 8;

/// Enum describing an output: file, STDOUT or STDERR
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum OutType<P> {
    /// Path to a file
    Path(P),
    /// STDOUT
    Stdout,
    /// STDERR
    Stderr,
}

impl<P: std::fmt::Debug> fmt::Display for OutType<P> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            OutType::Path(path) => write!(f, "{:?}", path),
            OutType::Stdout => write!(f, "stdout"),
            OutType::Stderr => write!(f, "stderr"),
        }
    }
}

/// Struct managing an buffered output
/// can be either STDOUT or STDERR or an opened file
pub struct Out {
    /// Out Path [OutType]
    pub out_path: OutType<PathBuf>,
    /// The writer
    pub out: Box<dyn Write>,
}

impl Out {
    /// Create an [Out] struct
    /// and open out with the real value (STDOUT, STDERR or file)
    ///
    /// ```
    /// use std::{error::Error, io::Write};
    /// use seqcollapse::out::{Out,OutType};
    ///
    /// fn main() -> Result<(), anyhow::Error> {
    ///     let mut out = Out::create::<&str>(OutType::Stdout)?;
    ///     let mut fout = Out::create(OutType::Path("tmp.txt"));
    ///     let mut log = Out::create::<&str>(OutType::Stderr)?;
    ///
    ///     writeln!(log, "# start log");
    ///     Ok(())
    /// }
    /// ```
    pub fn create<P>(file_out: OutType<P>) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out_path: OutType<PathBuf> = match file_out {
            OutType::Path(path) => OutType::Path(path.into()),
            OutType::Stdout => OutType::Stdout,
            OutType::Stderr => OutType::Stderr,
        };
        // out start with stdout. out must be with created with method create_output()
        let out = Out {
            out_path,
            out: Box::new(io::stdout()),
        };
        out._create(None)
    }

    /// Create an [Out] struct with a defined capacity for the buffer
    /// and open out with the real value (STDOUT, STDERR or file)
    /// ```
    /// use std::{error::Error, io::Write};
    /// use seqcollapse::out::{Out,OutType};
    ///
    /// fn main() -> Result<(), anyhow::Error> {
    ///     let mut out = Out::with_capacity::<String>(OutType::Stderr, 8192)?;
    ///     // or
    ///     let mut out = Out::with_capacity::<&str>(OutType::Stderr, 8192)?;
    ///
    ///     writeln!(out, "# start out");
    ///     Ok(())
    /// }
    /// ```
    pub fn with_capacity<P>(file_out: OutType<P>, capacity: usize) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out_path: OutType<PathBuf> = match file_out {
            OutType::Path(path) => OutType::Path(path.into()),
            OutType::Stdout => OutType::Stdout,
            OutType::Stderr => OutType::Stderr,
        };
        // out start with stdout. out must be with created with method create_output()
        let out = Out {
            out_path,
            out: Box::new(io::stdout()),
        };
        out._create(Some(capacity))
    }

    /// Create an [Out] with buffering.
    /// Can be either STDOUT or a file depending on the value of the `file_out` Option
    /// An optional capacity for the buffer must be specified
    ///
    /// If file_out is None use STDOUT
    /// ```
    /// use std::{error::Error, io::Write};
    /// use seqcollapse::out::Out;
    ///
    /// fn main() -> Result<(), anyhow::Error> {
    ///     // create an output to a file with default buffer size
    ///     let fout: String = "out.log".into();
    ///     let mut out = Out::create_file_or_stdout(Some(fout), None)?;
    ///     // or to Stdout with a specified buffer size
    ///     let mut out =  Out::create_file_or_stdout::<&str>(None, Some(8192))?;
    ///
    ///     writeln!(out, "# start log");
    ///     Ok(())
    /// }
    /// ```
    pub fn create_file_or_stdout<P>(
        file_out: Option<P>,
        opt_capacity: Option<usize>,
    ) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out = match file_out {
            Some(file_out) => Out {
                out_path: OutType::Path(file_out.into()),
                out: Box::new(io::stdout()),
            },
            None => Out {
                out_path: OutType::Stdout,
                out: Box::new(io::stdout()),
            },
        };
        out._create(opt_capacity)
    }

    /// Create an [Out] with buffering.
    /// Can be either STDERR or a file depending on the value of the `file_out` Option
    /// An optional capacity for the buffer must be specified
    ///
    /// If file_out is None use STDERR
    /// ```
    /// use std::{error::Error, io::Write};
    /// use seqcollapse::out::Out;
    ///
    /// fn main() -> Result<(), anyhow::Error> {
    ///     // create an output to a file with default buffer size
    ///     let flog: String = "out.log".into();
    ///     let mut log = Out::create_file_or_stderr(Some(flog), None)?;
    ///     // or to Stderr with a specified buffer size
    ///     let mut log =  Out::create_file_or_stderr::<&str>(None, Some(8192))?;
    ///
    ///     writeln!(log, "# start log");
    ///     Ok(())
    /// }
    /// ```
    pub fn create_file_or_stderr<P>(
        file_out: Option<P>,
        opt_capacity: Option<usize>,
    ) -> std::io::Result<Out>
    where
        PathBuf: From<P>,
    {
        let out = match file_out {
            Some(file_out) => Out {
                out_path: OutType::Path(file_out.into()),
                out: Box::new(io::stdout()),
            },
            None => Out {
                out_path: OutType::Stderr,
                out: Box::new(io::stdout()),
            },
        };
        out._create(opt_capacity)
    }

    /// _create: open `Out.out` with buffering accordingly to the value of `Out.out_path`
    /// A optional capacity must be specified.
    /// If the value is None, a default capacity is configured
    fn _create(mut self, opt_capacity: Option<usize>) -> std::io::Result<Out> {
        //Some(ref path) => File::open(path).map(|f| Box::new(f) as Box<dyn Write>),
        let capacity = match opt_capacity {
            Some(capacity) => capacity,
            None => OUT_DEFAULT_BUFSIZE,
        };
        match &self.out_path {
            //if let OutType::Path(path) = &self.out_path {
            OutType::Path(path) => {
                let new_out = File::create(path); //as Box<dyn Write>;
                match new_out {
                    Ok(out) => {
                        self.out =
                            Box::new(BufWriter::with_capacity(capacity, out)) as Box<dyn Write>
                    }
                    Err(e) => {
                        return Err(e);
                    }
                };
            }
            OutType::Stdout => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stdout()))
            }
            OutType::Stderr => {
                self.out = Box::new(BufWriter::with_capacity(capacity, io::stderr()))
            }
        }
        Ok(self)
    }
}

impl fmt::Debug for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}

impl fmt::Display for Out {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Out")
            .field("out_path", &self.out_path)
            .finish()
    }
}

impl io::Write for Out {
    fn write(&mut self, buf: &[u8]) -> io::Result<usize> {
        self.out.write(buf)
    }

    fn flush(&mut self) -> io::Result<()> {
        self.out.flush()
    }
}
