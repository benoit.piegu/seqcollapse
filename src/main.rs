/*  seqcollapse
 *  Author: Benoît Piégu, 2022-2024
 *
 * ---------------------------------------------------------------------------
 * seqcollapse is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 * or see the on-line version at http://www.gnu.org/copyleft/gpl.txt
 *
 */

use anyhow::bail;
use ::seqcollapse::seqcollapseconf::SeqCollapseConfBuilder;
use bumpalo::Bump;
use clap::{arg, builder::styling, command, crate_authors, crate_description};

// the template for help
pub const HELP_TEMPLATE: &str = "\
{before-help}{name} (v{version})
{author}

{about-with-newline}
{usage-heading} {usage}

{all-args}{after-help}

";

fn get_name_and_version(app: &clap::Command) -> String {
    let name = app.get_name();
    let version = app.get_version();
    let mut name = name.to_string();
    if let Some(version) = version {
        name.push_str(" v");
        name.push_str(version)
    }
    name
}

fn main() -> core::result::Result<(), anyhow::Error> {
    let styles = styling::Styles::styled()
        .header(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .usage(styling::AnsiColor::Green.on_default() | styling::Effects::BOLD)
        .literal(styling::AnsiColor::Blue.on_default() | styling::Effects::BOLD)
        .placeholder(styling::AnsiColor::Cyan.on_default());

    let app = command!()
        .about(crate_description!())
        .author(crate_authors!(", "))
        .styles(styles)
        .help_template(HELP_TEMPLATE)
        .arg_required_else_help(true)
        .arg(
            arg!(-d --debug ... "Sets the level of debugging")
                .required(false)
                .action(clap::ArgAction::Count),
        )
        .arg(
            arg!(--tag_regex ... "Regex to transform fastx in a useful tag")
            .value_parser(clap::value_parser!(String))
            .action(clap::ArgAction::Set)
            .required(false)
            .long_help(
"Allows you to specify a regular expression (perl syntax) to extract \
a label for each input file. These labels are used in the --stat or \
--stat2 outputs. Example: --tag_regex '/(\\w+)\\.fa$'\
"
            ),
        )
        .arg(
            arg!(-t --test_conf "No run : only test configuration")
                .required(false)
                .action(clap::ArgAction::SetTrue),
        )
        .next_help_heading("Options about sequences")
        .arg(
            arg!(-i --ignore_case "Ignore case distinctions in sequences")
            .required(false)
            .action(clap::ArgAction::SetTrue),
        )
        .arg(
            arg!(-r --revcomp "Test the the two strands")
                .required(false)
                .action(clap::ArgAction::SetTrue),
        )
        .next_help_heading("Output options")
        .arg(
            arg!(-a --add_count "Add count detected in sequence id")
                .required(false)
                .action(clap::ArgAction::SetTrue)
                .long_help(
"With the option --add (-a) is used, seqcollapse tries to identify \
a number of sequences at the end of the identifier of the id. In this \
case all the previous counts are summed.
To extract the sequence number, seqcollapse looks at the end of the \
sequence id, the '-' separator followed by a number. Example: for P10B-3895 \
=> 3895\
"
            ),
        )
        .arg(
            arg!(-j --json_log "JSON log rather than standard print")
                .required(false)
                .action(clap::ArgAction::SetTrue),
        )
        .arg(
            arg!(-s --stat "Stat by collapsed sequences")
                .required(false)
                .action(clap::ArgAction::SetTrue)
                .long_help(
"with --stat, for each final collapsed sequence returns (on STDERR by \
default) the new id for clustered sequences, the original bank, the number \
of original sequences in this bank, and a normalized number and this for \
each bank were the sequence lie. The format is:
`new_id`\t`bank`\t`nseq`\t`nseq_norm`
(separated by tabulation).\
"
                ),
        )
        .arg(
            arg!(-S --stat2 "Stat by original sequences")
                .required(false)
                .action(clap::ArgAction::SetTrue)
                .conflicts_with("stat")
                .long_help(
"with --stat2, for each final collapsed sequence returns (on STDERR \
by default) the new id for clustered sequences, the bank, the original \
number of sequence (deduced from the id) and  the normalized \
number of sequences and this for each original sequence. The format is: \
`new_id`\t`bank`\t`old_id`\t`old_nseq`\t`nseq`\t`nseq_norm`
(separated by tabulation).\
"
            ),
        )
        .arg(
            arg!(--factor_norm_freq <f> "Normalized number of sequences per bank")
                .value_parser(clap::value_parser!(f32))
                .action(clap::ArgAction::Set)
                .required(false)
                .long_help(
"Specifies the normalized number of sequences for a bank. Default value is 1e5."
            ),
        )
        .next_help_heading("Output files")
        .arg(
            arg!(-o --out <OUT_FILE> "out file for collapsed sequences")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .arg(
            arg!(-J --out_jaccard <OUT_JACCARD> "jaccard similarity output file")
                    .value_parser(clap::value_parser!(String))
                    .action(clap::ArgAction::Set)
                    .required(false),
        )
        .arg(
            arg!(-l --out_log <OUT_LOG> "log output file")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .arg(
            arg!(-O --out_stat <OUT_STAT> "stat output file")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .required(false),
        )
        .next_help_heading("Input files")
        .arg(
            arg!(<FASTX> ... "Sets FASTX files to read")
                .value_parser(clap::value_parser!(String))
                .action(clap::ArgAction::Set)
                .num_args(clap::builder::ValueRange::new(1..)),
        );

    let name_and_v = get_name_and_version(&app);
    let arguments = &app.get_matches();
    let debug = arguments.get_count("debug");
    seqcollapse::set_debug(debug.into());
    let only_test = arguments.get_flag("test_conf");
    let with_add = arguments.get_flag("add_count");
    let with_ignore_case = arguments.get_flag("ignore_case");
    let with_revcomp = arguments.get_flag("revcomp");
    let with_stat = arguments.get_flag("stat");
    let with_stat2 = arguments.get_flag("stat2");
    let json_log = arguments.get_flag("json_log");
    let factor_norm_freq = *arguments
        .get_one::<f32>("factor_norm_freq")
        .unwrap_or(&seqcollapse::DEFAULT_FACTOR_NORM_FREQ);
    /*
    eprintln!(
        "# {} --  with_add: {}, with_stat: {}, with_stat2: {}, debug: {}",
        name_and_v, with_add, with_stat, with_stat2, debug
    );
    */
    let fseqs: Vec<String> = arguments
        .get_many::<String>("FASTX")
        .unwrap()
        .filter(|f|{
            let file_size = std::fs::metadata(f).expect("file metadata not found").len();
            if file_size == 0 {
                eprintln!("# file <{}> is empty", f);
                return false;
            }
            true
        })
        .cloned()
        .collect();
    if fseqs.is_empty() {
        bail!("There is no file specified with data");
    }
    let stat_level = if with_stat2 {
        seqcollapse::StatLevel::SeqStat2
    } else if with_stat {
        seqcollapse::StatLevel::SeqStat
    } else {
        seqcollapse::StatLevel::None
    };
    let fastxtag_regex = arguments.get_one::<String>("tag_regex").cloned();
    let mut conf = SeqCollapseConfBuilder::default()
        .name_and_v(name_and_v)
        .stat_level(stat_level)
        .with_add(with_add)
        .with_ignore_case(with_ignore_case)
        .with_revcomp(with_revcomp)
        .fastx_files(fseqs)
        .fastxtag_regex(fastxtag_regex)
        .factor_norm_freq(factor_norm_freq)
        .build()?;
    conf.validate_tags()?;
    if only_test {
        if let Some(tag_regexp) = conf.fastxtag_regex {
            eprintln!("\n# tag_regex=<{tag_regexp}>\n#");
           for (fname, tag) in conf.fastx_files.iter().zip(conf.fastx_tag.iter()){
                eprintln!("#\t<{fname}> => {tag}");
           }
        }
        return Ok(());
    }
    let bump = Bump::with_capacity(1024 * 1024 * 8);
    let mut collapser = conf
        .init_seqcollapse_with_capacity::<seqcollapse::MyHasher, seqcollapse::USeqCount>(&bump, 1024 * 1024);
    let out = arguments.get_one::<String>("out").cloned();
    let out_stat = arguments.get_one::<String>("out_stat").cloned();
    let out_jaccard = arguments.get_one::<String>("out_jaccard").cloned();
    let mut out_log =
        seqcollapse::out::Out::create_file_or_stderr(arguments.get_one::<String>("out_log"), None)?;
    let run = collapser.out_collapsed_seq(out.as_deref(), out_stat.as_deref(), out_jaccard.as_deref())?;
    if json_log {
        // params and stat in json format
        writeln!(out_log.out, "{}", serde_json::to_string_pretty(&run)?)?;
    } else {
        writeln!(out_log.out, "{}", &run.params)?;
        writeln!(
            out_log.out,
            "# n seq = {} => n distinct seq = {} -- n singletons = {} -- nmax = {}",
            run.stat.n_seq_tot, run.stat.n_distinct_seq, run.stat.n_singleton, run.stat.clsize_max
        )?;
    }
    Ok(())
}
