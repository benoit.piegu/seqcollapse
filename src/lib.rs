use atoi::FromRadix10;
// use atoi_simd::parse;
use funty::{AtLeast32, AtMost64, Unsigned};
use lazy_static::lazy_static;
use lexical_core::ToLexical;
use memchr::{memchr, memrchr};
use needletail::{parser::SequenceRecord, Sequence};
use serde::Serialize;
use std::{
    borrow::Cow,
    collections::HashMap,
    fmt,
    hash::Hash,
    mem::MaybeUninit,
    ops::Deref,
    sync::atomic::{AtomicU64, Ordering},
};
use std::hash::BuildHasher;
pub mod inc_id;
pub mod jaccard_index;
pub mod out;
pub mod seqcollapse;
pub mod seqcollapseconf;


/// wraps [foldhash::fast::FoldHasher] to implement the Default trait
pub struct FoldHasher(foldhash::fast::FoldHasher);

impl std::hash::Hasher for FoldHasher {
    fn finish(&self) -> u64 {
        self.0.finish()
    }

    fn write(&mut self, bytes: &[u8]) {
        self.0.write(bytes)
    }
}

impl Default for FoldHasher {
    fn default() -> Self {
        let state = foldhash::fast::FixedState::default();
        FoldHasher(state.build_hasher())
    }
}

/// Set the hasher function used. [`ahash`] is the fastest. cf the bench
pub type MyHasher = ahash::AHasher;
// pub type MyHasher = FoldHasher;

/// A trait allowing sequence counting: an unsigned integer ([funty::Unsigned])
/// at least [u32] ([funty::AtLeast32]) and at most [u64] ([funty::AtMost32])
/// that can be parsed and written by [lexical_core].
/// This trait is used wherever sequence counts are involved.
pub trait SeqCount: Unsigned + AtLeast32 + AtMost64 + ToLexical + FromRadix10 {}
impl<T> SeqCount for T where T: Unsigned + AtLeast32 + AtMost64 + ToLexical + FromRadix10, {}

/// Sets the type to use for counting the number of sequences. Either u32 or u64.
/// Less would not be enough
///
/// `MAX: u32 = 4_294_967_295u32                  // 4.29e9`
/// `MAX: u64 = 18_446_744_073_709_551_615u64     // 1.18e19`
pub type USeqCount = u32;

/// Default factor to normalize counts per bank
pub const DEFAULT_FACTOR_NORM_FREQ: f32 = 1e5;


// thread_local!(pub static DEBUG: RefCell<u64> = RefCell::new(0));
pub static DEBUG: AtomicU64 = AtomicU64::new(0);

const INPUT_DEFAULT_BUFSIZE: usize = 1024 * 32;
pub const _COUNT_DELIM: [u8; 1] = [b'-'];
lazy_static! {
    /// A vector containing ALPHABET used to create new id of sequences. Use
    /// all capital letters plus numbers `(A..Z, 0..9)`.
    pub static ref ALPHABET: Vec<u8> = (b'A'..=b'Z').chain(b'0'..=b'9').collect();
    /// A str representing ALPHABET used to create new id of sequences
    pub static ref ALPHABET_STR: &'static str = unsafe { std::str::from_utf8_unchecked(&ALPHABET)};
    /// The length of ALPHABET
    pub static ref L_ALPHABET: usize = ALPHABET.len();
    /// A Hashmap allowing to get the index of a member of ALPHABET
    static ref H_LETTER2I:  HashMap<u8, u8> = ALPHABET
        .iter()
        .enumerate()
        .map(|(count, c)|(*c, count as u8))
        .collect();
    /// A vector to obtain the index of a member of the alphabet
    static ref LETTER2I: Vec<u8> = (0..=u8::MAX)
        .map(|k| {
            let v = H_LETTER2I.get(&k).or(Some(&b'.'));
            *v.unwrap()
        })
        .collect();
    /// The delimitator between id of sequence and the count of sequence
    pub static ref COUNT_DELIM: Vec<u8> = vec![b'-'];
    // pub static ref COUNT_DELIM: [u8; 1] = [b'-'];
    /// The maximum number of signs possible using ALPHABET and with the
    /// [USeqCount] type to represent the sequence count
    pub static ref MAX_NSIGNS: usize =
        (USeqCount::MAX as f32 * ALPHABET.len() as f32 - USeqCount::MAX as f32 + 1f32)
            .log(ALPHABET.len() as f32) as usize - 1;
}

pub fn get_debug() -> u64 {
    DEBUG.load(Ordering::SeqCst) //Relaxed)
}

pub fn set_debug(level: u64) {
    DEBUG.store(level, Ordering::SeqCst); //Relaxed);
}

/// Level of stat for Seqcollapse
#[derive(Debug, Clone, Copy, Default, Serialize)]
pub enum StatLevel {
    /// no stat
    #[default]
    None,
    /// stat by collapsed seq
    SeqStat,
    /// stat by original seq
    SeqStat2,
}

impl fmt::Display for StatLevel {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match self {
            StatLevel::None => write!(f, "None"),
            StatLevel::SeqStat => write!(f, "Stat"),
            StatLevel::SeqStat2 => write!(f, "Stat2"),
        }
    }
}

/// get id of a sequence from the header line (fasta/fastq formats)
///
/// # Arguments
///
/// * seq_header - the seq header as an `&[u8]`
///
/// # Examples
///
/// ```
/// use seqcollapse::get_seq_id;
///
/// let seq_header = b"AB-12 1:N:0:ATTCCT";
/// let seq_id = get_seq_id(seq_header);
/// assert_eq!(seq_id, &seq_header[0..5]);
/// ```
///
#[inline(always)]
pub fn get_seq_id(seq_header: &[u8]) -> &[u8] {
    let pos_end_of_id = memchr(b' ', seq_header).unwrap_or(seq_header.len());
    &seq_header[0..pos_end_of_id]
}

/// get count from sequences id formatted as : id-count
///
/// # Examples
///
/// ```
/// use seqcollapse::get_seq_count;
///
/// let seq_id = b"AB-12";
/// let seq_count = get_seq_count::<usize>(seq_id);
/// assert_eq!(seq_count, 12);
/// 
/// let seq_id = b"AB-a";
/// let seq_count = get_seq_count::<usize>(seq_id);
/// assert_eq!(seq_count, 1);
/// ```
///
#[inline(always)]
pub fn get_seq_count<T>(seq_header: &[u8]) -> T
where
    T: Unsigned + FromRadix10,
{
    let seq_id = get_seq_id(seq_header);
    if let Some(pos_minus) = memrchr(b'-', seq_id) {
        // if pos_minus < pos_end_of_id -1 {
        if pos_minus < seq_id.len() - 1 {
            //let c = atoi::atoi::<usize>(&seq_id[pos_minus + 1..]).unwrap_or(1);
            // let c = atoi_simd::parse::<usize>(&seq_id[pos_minus + 1..]).unwrap_or(1);
            // return T::try_from(c).unwrap_or(T::ONE);
            let (c, l) = T::from_radix_10(&seq_id[pos_minus + 1..]);
            if l == 0 { return T::ONE; } else { return c; }
        }
    }
    T::ONE
}

// Roughly equivalent to the function needletail::sequence::canonical()
// From a sequence, return the lightest form of the sequence
// (the lexicographically lowest of either the original sequence or its
// reverse complement)
fn _ligthtest_strand<'a>(seqrec: &'a SequenceRecord<'a>) -> Cow<'a, [u8]> {
    let seq = seqrec.seq();
    let revcomp = seqrec.reverse_complement();
    if seq.deref() < &revcomp[..] {
        seq
    } else {
        revcomp.into()
    }
}

/// Create a stable id from a number using a defined alphabet ([`static@ALPHABET``])
///
/// # Arguments
///
/// * `n` - the number to convert to an id
///
/// # Examples
///
/// ```
/// use seqcollapse::n_to_id;
///
/// let v: Vec<u8> = n_to_id::<usize>(36, 1);
/// assert_eq!(v, b"AA-1".to_vec());
/// ```
///
pub fn n_to_id<T>(n: usize, count: T) -> Vec<u8>
where
    T: Unsigned + AtLeast32,
{
    #[cfg(debug)]
    let debug = get_debug();
    let l_alpha = ALPHABET.len();
    let n_tmp = n + 1;
    // 'l' = length of alphabet
    // 'nsigns' = number of signs
    // 'N' = number of possible combination for 'n signs'
    // N = l + l² + l³ + ... + l^nsigns
    // N = (l^(nsigns+1)-1)/(l-1)
    // nsigns = log_l(N.l-N + 1) - 1
    let nsigns = ((n_tmp * l_alpha - n_tmp + 1) as f32).log(l_alpha as f32) as usize - 1;
    let s_count = count.to_string();
    // rank = 0: one symbol + '-'.len() +  s_count.len()
    let mut v: Vec<MaybeUninit<u8>> = Vec::with_capacity(nsigns + 2 + s_count.len());
    unsafe {
        v.set_len(nsigns + 1);
    }
    #[cfg(debug)]
    if debug > 0 {
        eprintln!("#    i={} rank={} l_alphabet={}", n, nsigns, l_alpha);
    }
    let mut n_tmp = n;
    for rank in (0..=nsigns).rev() {
        let val = n_tmp % l_alpha;
        v[rank].write(ALPHABET[val]);
        if rank == 0 {
            break;
        }
        n_tmp = (n_tmp - val) / l_alpha - 1;
    }
    let mut v: Vec<u8> = unsafe { std::mem::transmute(v) };
    v.push(b'-');
    let s_count = count.to_string();
    v.extend_from_slice(s_count.as_bytes());
    #[cfg(debug)]
    if debug > 0 {
        eprintln!("#    i: {} =>id={}", n, String::from_utf8_lossy(&v[..]));
    }
    v
}

/// return which is not unique by vector
/// # Arguments
///
/// * v_string: a ref on a vector of string
///
/// # Examples
///
/// ```
/// use seqcollapse::get_no_unique;
///
/// let v: Vec<String> = vec!["a", "b", "c"].iter().map(|&s| s.into()).collect();
/// assert_eq!(get_no_unique(&v), None);
///
/// ```
///
/// ```
/// use seqcollapse::get_no_unique;
///
/// let v = vec!["a", "b", "c", "a", "d", "c", "e", "c"];
/// let res = get_no_unique(&v);
/// assert_eq!(res, Some(vec![vec![0usize, 3], vec![2, 5, 7]]));
/// ```
///
pub fn get_no_unique<T>(vec: &[T]) -> Option<Vec<Vec<usize>>>
where
    T: Eq,
    T: PartialEq,
    T: Hash,
{
    let mut h = HashMap::<&T, Vec<usize>, std::hash::BuildHasherDefault<MyHasher>>::default();
    let mut l_max: usize = 1;
    vec.iter().enumerate().for_each(|(i, elem)| {
        if let Some(v) = h.get_mut(&elem) {
            v.push(i);
            l_max = l_max.max(v.len());
        } else {
            h.insert(elem, vec![i]);
        }
    });
    if l_max > 1 {
        let res = vec
            .iter()
            //.into_iter()
            .filter_map(|elem: &T| h.remove(&elem))
            .filter_map(|v| if v.len() > 1 { Some(v.clone()) } else { None })
            .collect::<Vec<Vec<usize>>>();
        Some(res)
    } else {
        None
    }
}



