
use std::marker::PhantomData;

use anyhow::bail;
use lexical_core::BUFFER_SIZE;

use crate::{SeqCount, ALPHABET, COUNT_DELIM, LETTER2I, MAX_NSIGNS};

/// A structure for incrementally building ids from a defined alphabet ([static@ALPHABET])
/// The maximum number of ids is equal to `u64::MAX` (1.84e+19)
///
/// [IncId] is parametrized by `T`, a type representing the count of sequence.
/// Therefore, `T` must be an unsigned integer. Possible types are restricted
/// (at least `u32`, at most `u64`).
///
/// Note that [IncId] uses [lexical_core::write()] to perform the conversion
/// from count to string.
///
/// This struct contains a buffer containing the current id. The ids are
///  obtained by successive increments with function [IncId::next_id()] or
/// [IncId::next_id_with_count()].
///
/// ```rust
/// use seqcollapse::L_ALPHABET;
/// use seqcollapse::inc_id::IncId;
///
/// let mut inc_id = IncId::<u32>::new();
/// let id = inc_id.next_id().unwrap();
/// assert_eq!(id, b"A".to_vec());
/// assert_eq!(inc_id.i, 1);
///
/// // increment the IndIc of the length of the alphabet used
/// (1..*L_ALPHABET).for_each(|i| { inc_id.next_id(); });
///
/// let id = inc_id.next_id().unwrap();
/// assert_eq!(id, b"AA".to_vec());
/// assert_eq!(inc_id.i, *L_ALPHABET+1);
///
/// let id = inc_id.next_id_with_count(120).unwrap();
/// assert_eq!(id, b"AB-120".to_vec());
/// ```
///
pub struct IncId<T> {
    /// buff contain the last id with count:
    /// ```text
    /// [.......nsigns_max][delim][count]
    ///     ^              ^
    ///     <- id_start    id_end
    ///                           ^   <- count_end (calculated in next_id_with_count)
    ///                           count_start (constant)
    /// ```
    /// Current id is slice buffer[start..end] and the id increases towards the
    /// beginning of the buffer. After is the delimiter and after the count.
    /// id_start decrease. id_end and count_start are constant.
    buff: Vec<u8>,
    /// index of the start of the id
    id_start: usize,
    /// index of the end of the id
    id_end: usize,
    /// index of the start of the count
    count_start: usize,
    /// id number
    pub i: usize,
    /// Marker for the type of count, T
    phantom: PhantomData<T>,
}

impl <T> std::fmt::Debug for IncId<T> {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let _ = f.write_fmt(format_args!("id={}[{}]{}  ",
            unsafe { &std::str::from_utf8_unchecked(&self.buff[0..self.id_start], ) },
            unsafe { &std::str::from_utf8_unchecked(&self.buff[self.id_start..self.id_end], ) },
            unsafe { &std::str::from_utf8_unchecked(&self.buff[self.id_end..self.count_start], ) }
        ));
        f.debug_struct("IncId")
            .field("buff",   unsafe { &std::str::from_utf8_unchecked(&self.buff[..self.count_start]) })
            .field("id_start", &self.id_start)
            .field("id_end", &self.id_end)
            .field("count_start", &self.count_start)
            .field("i", &self.i)
            .finish()
    }
}

impl<T> IncId<T>
where
    T: SeqCount, {

    /// create a new IncId parameterized by `T`
    /// `T` must be an unsigned integer (at least u32, at most u64)
    pub fn new() -> IncId<T> {
        let buff_size = *MAX_NSIGNS + COUNT_DELIM.len() + BUFFER_SIZE;
        // * (Fundamental::as_f32(T::MAX)).log10().floor() as usize;
        let mut buff = vec![ALPHABET[0]; buff_size];
        buff[*MAX_NSIGNS..(*MAX_NSIGNS+COUNT_DELIM.len())].copy_from_slice(&COUNT_DELIM);
        IncId::<T> {
            buff,
            id_start: *MAX_NSIGNS,
            id_end: *MAX_NSIGNS,
            count_start: *MAX_NSIGNS + COUNT_DELIM.len(),
            i: 0,
            phantom: PhantomData,
        }
    }

    #[allow(clippy::needless_return)]
    #[inline(always)]
    fn _inc_id(&mut self, id_end: usize) -> Result<&[u8], anyhow::Error>  {
        self.i += 1;
        // fist inc!
        if self.id_start == self.id_end {
            self.id_start -= 1;
            return Ok(&self.buff[self.id_start..id_end]);
        }
        for rank in (self.id_start..self.id_end).rev() {
            let letter = self.buff[rank];
            let iletter = LETTER2I[letter as usize] as usize;
            //    .with_context(||format!("No character <{}>in Alphabet", letter as char))? as usize;
            if iletter < ALPHABET.len() - 1 {
                self.buff[rank] = ALPHABET[iletter + 1];
                return Ok(&self.buff[self.id_start..id_end]);
            } else {
                // last letter in alphabet for this rank,
                // the precedent rank must be incremented
                self.buff[rank] = ALPHABET[0];
            }
        }
        if self.id_start > 0 {
            self.id_start -= 1;
            return Ok(&self.buff[self.id_start..id_end]);
        } else {
            eprintln!("{:?}", self);
            bail!(format!("IncId buffer capacity exhausted -- count={}", self.i));
        }
    }

    /// Creation of the next id by incrementing the previous one
    #[inline(always)]
    pub fn next_id(&mut self) -> Result<&[u8], anyhow::Error> {
        self._inc_id(self.id_end)
    }

    /// Creation of the next id by incrementing the previous one
    /// To the id is added [static@COUNT_DELIM] and the sequence count
    #[inline(always)]
    pub fn next_id_with_count(&mut self, count: T) -> Result<&[u8], anyhow::Error> {
        // format count number with lexical_core
        let s_count = lexical_core::write(count, &mut self.buff[self.count_start..]);
        let end_with_count = self.count_start + s_count.len();
        self._inc_id(end_with_count)
    }

}

impl<T> Default for IncId<T>
where
    T: SeqCount,
{
    fn default() -> Self {
        Self::new()
    }
}
