//! Performs sequence collapse
//!
//! Three structs implement trait [SeqCollapse]
//! * [HSeqCount]: simple collapse (corresponding to [crate::StatLevel::None])
//! * [HSeqStat]: collapse sequences and write an output with the number of sequences in input banks by collapsed sequences. For this, must store the number of input sequence by bank by collapsed sequence (correspondnig to [crate::StatLevel::SeqStat]).
//! * [HSeqStat2]: collapse sequences and write an output with what sequences in input banks produced the collapsed sequences. For this, must store the all sequences id corresponding by collapsed sequence (corresponding to [crate::StatLevel::SeqStat2])
//!
//! This three types embed a [crate::seqcollapseconf::SeqCollapseConf] struct
//! and a [SeqCollapseStat]
//!
//! [BoxedSeqCollapse] struct wrap a `dyn` [SeqCollapse] and manage the
//! [BoxedSeqCollapse] traits and handles the creation of the underlying
//! struct ([HSeqCount], [HSeqStat] or [HSeqStat2]), the collapsing of
//!  sequences ([SeqCollapse::collapse_seq()]) and the creation of outputs
//! ([SeqCollapse::out_collapsed_seq()]) and possibly [SeqCollapse::out_jaccard()].
//!
//!
//! The struct [Run] embed a [crate::seqcollapseconf::SeqCollapseConf] and a [SeqCollapseStat] structs.
//!
use anyhow::{anyhow,Context};
use bumpalo::Bump;
use funty::Fundamental;
use itertools::Itertools;
use needletail::{
    parse_fastx_reader,
    parser::{write_fasta, LineEnding},
    sequence::canonical,
    FastxReader,
};
use serde::Serialize;
use std::{
    borrow::Cow,
    collections::HashMap,
    fs::File,
    hash::{BuildHasher, BuildHasherDefault, Hash, Hasher},
    io::{self, BufReader, Read, Write},
    marker::PhantomData,
    ops::Deref,
};

#[cfg(debug)]
use std::sync::atomic::Ordering;

use crate::{
    get_seq_count, get_seq_id, inc_id::IncId, jaccard_index::{cmp_count_wo_singleton, JaccardIndex}, out::{self, Out}, seqcollapseconf::SeqCollapseConf, SeqCount, StatLevel, INPUT_DEFAULT_BUFSIZE
};

#[cfg(debug)]
use crate::DEBUG;

type BankID = u16;

/// A structure to store some stats after a collapse.
#[derive(Clone, Debug, Serialize)]
pub struct SeqCollapseStat {
    /// total number of sequences taking into account sequence counts
    pub n_seq_tot: usize,
    /// number of distinct sequences
    pub n_distinct_seq: usize,
    /// number of distinct sequences with only one representative (cluster size=1)
    pub n_singleton: usize,
    /// maximum size of a cluster of identical sequence
    pub clsize_max: usize,
    /// vector containing the total number of sequences taking into account sequence counts per bank
    pub v_nseq_tot: Vec<usize>,
}

impl SeqCollapseStat {
    fn new(nbanks: usize) -> Self {
        SeqCollapseStat {
            n_seq_tot: 0,
            n_distinct_seq: 0,
            n_singleton: 0,
            clsize_max: 1,
            v_nseq_tot: vec![0; nbanks],
        }
    }

    fn set_v_nseq_tot(&mut self, v: Vec<usize>) -> Result<(), anyhow::Error> {
        if v.len() != self.v_nseq_tot.len() {
           return Err(
            anyhow!("The update of the sequence count per library is impossible: the vector length is incorrect. Expected {}, but got {}",
            self.v_nseq_tot.len(), v.len())
           );
        }
        self.v_nseq_tot = v;
        self.n_seq_tot = self.v_nseq_tot.iter().sum();
        Ok(())
    }

}

/// A struct storing a collapse run: the conf (a [SeqCollapseConf]) and the result [SeqCollapseStat]
#[derive(Debug, Serialize)]
pub struct Run {
    pub params: SeqCollapseConf,
    pub stat: SeqCollapseStat,
}

trait HashMapExt {
    // fn new() -> Self;
    fn with_capacity(x: usize) -> Self;
}

impl<K, V, S> HashMapExt for HashMap<K, V, S>
where
    K: Hash + Eq,
    S: BuildHasher + Default,
{
    /* fn new() -> Self {
        HashMap::with_hasher(S::default())
    }
    */

    fn with_capacity(capacity: usize) -> Self {
        HashMap::with_capacity_and_hasher(capacity, S::default())
    }
}

/// [BoxedSeqCollapse] encapsulate a struct implementing the trait [SeqCollapse].
///
/// This structure can be created via [SeqCollapseConf] with the function
///  [SeqCollapseConf::init_seqcollapse]
pub struct BoxedSeqCollapse<'a, H, T>(Box<dyn SeqCollapse<T> + 'a>, PhantomData<&'a H>);

impl<'a, H, T> BoxedSeqCollapse<'a, H, T>
where
    T: SeqCount,
    //usize: From<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    /// create a [BoxedSeqCollapse] from a [SeqCollapseConf]. The boxed struct is
    ///  chosen according to the value of the [StatLevel] enum
    pub fn new(conf: SeqCollapseConf, bump: &'a Bump) -> BoxedSeqCollapse<'a, H, T> {
        let hseq = match conf.stat_level {
            StatLevel::None => {
                Box::new(HSeqCount::<H, T>::new(bump, conf)) as Box<dyn SeqCollapse<T>>
            }
            StatLevel::SeqStat => {
                Box::new(HSeqStat::<crate::MyHasher, T>::new(bump, conf)) as Box<dyn SeqCollapse<T>>
            }
            StatLevel::SeqStat2 => Box::new(HSeqStat2::<crate::MyHasher, T>::new(bump, conf))
                as Box<dyn SeqCollapse<T>>,
        };
        // let hseq: Box<HSeqStat<'a, ahash::AHasher, T>>= Box::new(HSeqStat::<'a, crate::MyHasher, T>::new(&bump, conf));
        BoxedSeqCollapse(hseq, PhantomData)
    }

    /// create a [BoxedSeqCollapse] from a [SeqCollapseConf] with a capacity. The boxed struct is
    ///  chosen according to the value of the [StatLevel] enum
    pub fn with_capacity(conf: SeqCollapseConf, bump: &'a Bump, capacity: usize) -> BoxedSeqCollapse<'a, H, T> {
        let hseq = match conf.stat_level {
            StatLevel::None => {
                Box::new(HSeqCount::<H, T>::with_capacity(bump, conf, capacity)) as Box<dyn SeqCollapse<T>>
            }
            StatLevel::SeqStat => {

                Box::new(HSeqStat::<crate::MyHasher, T>::with_capacity(bump, conf, capacity)) as Box<dyn SeqCollapse<T>>
            }
            StatLevel::SeqStat2 => Box::new(HSeqStat2::<crate::MyHasher, T>::with_capacity(bump, conf, capacity))
                as Box<dyn SeqCollapse<T>>,
        };
        // let hseq: Box<HSeqStat<'a, ahash::AHasher, T>>= Box::new(HSeqStat::<'a, crate::MyHasher, T>::new(&bump, conf));
        BoxedSeqCollapse(hseq, PhantomData)
    }

    /// creates the output of the collapsed sequences set
    pub fn out_collapsed_seq(
        &mut self,
        out: Option<&str>,
        out_stat: Option<&str>,
        out_jaccard: Option<&str>,
    ) -> Result<Run, anyhow::Error> {
        let stat_level = self.0.get_conf().stat_level;
        // open out files and writes the set of collapsed sequences
        let out = Out::create_file_or_stdout(out, None)
            .with_context(|| format!("couldn't open out file <{:?}>", &out))?;
        let out_stat = match stat_level {
            StatLevel::None => None,
            StatLevel::SeqStat | StatLevel::SeqStat2 => Some(
                Out::create_file_or_stderr(out_stat, None)
                    .with_context(|| format!("couldn't open stat out file <{:?}>", &out_stat))?,
            ),
        };
        self.0.collapse_seq()?;
        let run = self.0.out_collapsed_seq(out, out_stat)?;
        if let Some(out_jaccard) = out_jaccard {
            let out = Out::create(out::OutType::Path(out_jaccard))?;
            self.0.out_jaccard(out)?;
        }
        Ok(run)
    }
}

/// A trait to perform sequences collapsing from FASTA/FASTQ files to create
///  a non redundoant set of sequences. The total count of a sequence are specified
///  in the id of sequences (after the [static@crate::COUNT_DELIM]). The generic type `<T>`
///  is used for sequences counting.
///
/// Manage three options: 
///
/// * add: tries to identify a number of sequences at the end of the identifier of
///   the id. In this case all counts for this sequence are summed.
/// * revcomp: test the the two strands of (DNA) sequances
/// * ignore_case: ignore case when comparing sequence
pub trait SeqCollapse<T>
where
    T: SeqCount,
{
    /// Get conf, a [SeqCollapseConf] struct.
    fn get_conf(&self) -> &SeqCollapseConf;

    /// Get the value of option [`SeqCollapseConf::with_add`].  If True, take into account counts detected in sequence id
    fn get_with_add(&self) -> bool {
        self.get_conf().get_with_add()
    }

    /// Get the value of option [`SeqCollapseConf::with_ignore_case`]. If True, ignore case when comparing sequence
    fn get_with_ignore_case(&self) -> bool {
        self.get_conf().with_ignore_case
    }

    /// Get the value of option [`SeqCollapseConf::with_revcomp`]. If True, rev comp sequences are also compared
    fn get_with_revcomp(&self) -> bool {
        self.get_conf().with_revcomp
    }

    /// Get the list of input files (a `Vec<String>`)
    fn get_fastx_files(&self) -> &Vec<String> {
        self.get_conf().get_fastx_files()
    }

    /// Get stat about collapse ([SeqCollapseStat])
    fn get_stat(&self) -> &SeqCollapseStat;

    /// Get a mutable [SeqCollapseStat] about collapse
    fn get_mut_stat(&mut self) -> &mut SeqCollapseStat;

    /// Insert a new sequence.
    fn ins_val(&mut self, seq: Cow<'_,[u8]>, bank_id: BankID, count: T, seq_header: &[u8]);

    /// Add sequences from a seqreader [`FastxReader`]
    /// manage with_add, ignore_case and revcomp options. Transform sequence
    ///  accordingly to with ignore_case and revcomp options.
    ///
    /// returns the number of sequences read.
    fn add_seq_from_reader(
        &mut self,
        seqreader: &mut Box<dyn FastxReader>,
        bankid: BankID,
    ) -> Result<usize, anyhow::Error> {
        // let mut nseq: usize = 0;
        let with_add = self.get_with_add();
        let ignore_case = self.get_with_ignore_case();
        let revcomp = self.get_with_revcomp();
        let mut nseq_tot: T = T::ZERO;
        while let Some(seqrec) = seqreader.next() {
            let seqrec = seqrec.with_context(|| "Error in fastx stream")?;
            // nseq += 1;
            let mut count: T = T::ONE;
            let seq_header = seqrec.id();
            if with_add {
                count = get_seq_count(seq_header);
            }
            nseq_tot += count;
            let seq = seqrec.seq();
            let mut k = seq;
            if ignore_case {
                k =  Cow::from(
                    k.iter()
                        .map(|c| c.to_ascii_uppercase())
                        .collect::<Vec<u8>>(),
                )
           }
           let k = if revcomp {
                canonical(&k)
            } else {
                k
            };
            self.ins_val(k, bankid, count, seq_header);
        }
        Ok(Fundamental::as_usize(nseq_tot))
    }

    fn get_n_distinct_seq(&self) -> usize;

    /// Collapse all sequences from Fastx files
    fn collapse_seq(&mut self) -> Result<(), anyhow::Error> {
        // let fastx_files:Vec<String> = self.get_fastx_files().iter().map(|f| f.clone()).collect();
        // let fastx_files:Vec<String> = self.get_fastx_files().iter().cloned().collect();
        let fastx_files: Vec<String> = self.get_fastx_files().to_vec();
        // let mut v_nseq: Vec<usize> = vec![0; self.get_conf().nbanks()];
        let mut v_nseq_tot: Vec<usize> = vec![0; self.get_conf().nbanks()];
        for (idbank, fastx_path) in (0 as BankID..).zip(fastx_files.iter()) {
            let buff = if fastx_path[..].eq("-") {
                let stdin = io::stdin();
                Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, stdin))
                    as Box<dyn io::Read + Send>
            } else {
                let file = File::open(fastx_path)
                    .with_context(|| format!("couldn't open fastx file <{}>", &fastx_path))?;
                Box::new(BufReader::with_capacity(INPUT_DEFAULT_BUFSIZE, file))
                    as Box<dyn Read + Send>
            };
            let mut seqreader = parse_fastx_reader(buff)
                .with_context(|| format!("can't parse <{}>", fastx_path))?;
            // eprint!("# format={:?} typeId={:?}", seqreader.type_of(), seqreader.type_id());
            let nseq_tot = self
                .add_seq_from_reader(&mut seqreader, idbank as BankID)
                .with_context(|| format!("can't parse fastx file <{}>", fastx_path))?;
                // v_nseq[idbank as usize] += nseq;
                v_nseq_tot[idbank as usize] += nseq_tot;
        }
        let n_distinct_seq = self.get_n_distinct_seq();
        let stat = self.get_mut_stat();
        // stat.v_nseq_tot = v_nseq;
        // stat.set_v_nseq_tot(v_nseq)?;
        stat.set_v_nseq_tot(v_nseq_tot)?;
        stat.n_distinct_seq = n_distinct_seq;
        Ok(())
    }

    /// Print the collapsed sequences
    fn out_collapsed_seq(&mut self, out: Out, stat_out: Option<Out>) -> Result<Run, anyhow:: Error>;

    /// return a jaccard index, [JaccardIndex] between all input files
    fn get_jaccard_index(&self) -> JaccardIndex;

    /// output the [JaccardIndex]
    fn out_jaccard(&self, mut out: Out) -> Result<(), anyhow::Error> {
        if self.get_conf().nbanks() < 2 {
            writeln!(out.out, "# Jaccard similarity calculation impossible if nbanks < 2")?;
            return Ok(())
        }
        let ji = self.get_jaccard_index();
        let tags = &self.get_conf().fastx_tag;
        let similarities = ji.similarity();
        writeln!(out.out, "bank1\tbank2\tsim")?;
        similarities
            .iter()
            .for_each(| ((i0, i1), s)| {
                let _res = writeln!(out.out, "{}\t{}\t{}", tags[*i0], tags[*i1], s);
                let _res = writeln!(out.out, "{}\t{}\t{}", tags[*i1], tags[*i0], s);
            });
        // let _res = writeln!(out, "# similarities = {:?}", &similarities);
        Ok(())
    }
}

// pub(crate) trait SeqCollapseU32: SeqCollapse<u32> {}
// pub(crate) trait SeqCollapseU64: SeqCollapse<u64> {}


pub(crate) type BumpVec<'a, T> = bumpalo::collections::Vec<'a, T>;

/// A struct to perform sequences collapsing.
///
/// The generic type `<T>` is used for sequences counting.
pub struct HSeqCount<'a, H, T> {
    bump: &'a Bump,
    conf: SeqCollapseConf,
    stat: SeqCollapseStat,
    h: HashMap<BumpVec<'a, u8>, T, BuildHasherDefault<H>>,
}

impl<'a, H, T> HSeqCount<'a, H, T>
where
    T: SeqCount,
    //usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    pub fn new(bump: &'a Bump, conf: SeqCollapseConf) -> HSeqCount<'a, H, T> {
        let nbanks = conf.nbanks();
        HSeqCount {
            bump,
            conf,
            stat: SeqCollapseStat::new(nbanks),
            h: HashMap::<BumpVec<u8>, T, BuildHasherDefault<H>>::default(),
        }
    }

    pub fn with_capacity(
        bump: &'a Bump,
        conf: SeqCollapseConf,
        capacity: usize,
    ) -> HSeqCount<'a, H, T> {
        let nbanks = conf.nbanks();
        HSeqCount {
            bump,
            conf,
            stat: SeqCollapseStat::new(nbanks),
            h: HashMap::<BumpVec<u8>, T, BuildHasherDefault<H>>::with_capacity(capacity),
        }
    }

    /// function to perform sequence collapsing from a buffer
    /// (for benchmark)
    pub fn collapse_seq_from_buff(&mut self, buff: &'static [u8]) -> Result<usize, anyhow::Error> {
        let mut tot_nseq: usize = 0;
        let mut seqreader =
            parse_fastx_reader(buff).with_context(|| "can't parse bugg".to_string())?;
        let nseq_tot = self.add_seq_from_reader(&mut seqreader, 0 as BankID)?;
        tot_nseq += nseq_tot;
        Ok(tot_nseq)
    }
}

impl<H, T> SeqCollapse<T> for HSeqCount<'_, H, T>
where
    T: SeqCount,
    //usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    fn get_conf(&self) -> &SeqCollapseConf {
        &self.conf
    }

    fn get_stat(&self) -> & SeqCollapseStat {
        &self.stat
    }

    fn get_mut_stat(&mut self) -> &mut SeqCollapseStat{
        &mut self.stat
    }

    #[allow(unused_variables)]
    fn ins_val(&mut self, seq: Cow<'_, [u8]>, bank_id: BankID, count: T, seq_header: &[u8]) {
        if let Some(new_count) = self.h.get_mut(seq.deref()) {
            *new_count += count;
        } else {
            let mut new_k = BumpVec::<u8>::with_capacity_in(seq.len(), self.bump);
            unsafe {
                new_k.set_len(seq.len());
            }
            new_k.copy_from_slice(&seq);
            self.h.insert(new_k, count);
        }
    }

    fn get_n_distinct_seq(&self) -> usize {
        self.h.len()
    }

    fn out_collapsed_seq(
        &mut self,
        mut out: Out,
        _stat_out: Option<Out>,
    ) -> Result<Run, anyhow::Error> {
        #[cfg(debug)]
        let debug = DEBUG.load(Ordering::Relaxed);
        let mut inc_id = IncId::<T>::new();
        let mut n_singleton = 0;
        let mut clsize_max = T::ZERO;
        self.h
            .iter()
            .sorted_unstable_by(|a, b| Ord::cmp(&b.1, &a.1))
            // .enumerate()
            // .for_each(|(iseq, (seq, count))| {
            .try_for_each(|(seq, count)| {
                #[cfg(debug)]
                if debug > 1 {
                    eprintln!("# {} {:?} => {}", iseq, String::from_utf8_lossy(seq), count);
                }
                if *count == T::ONE {
                    n_singleton += 1
                } else {
                    clsize_max = clsize_max.max(*count);
                }
                let new_id = inc_id.next_id_with_count(*count)?;
                write_fasta(new_id, seq, &mut out, LineEnding::Unix)?;
                Ok::<(), anyhow::Error>(())
            })?;
        self.stat.n_singleton = n_singleton;
        self.stat.clsize_max = Fundamental::as_usize(clsize_max);
        let run = Run {
            params: self.get_conf().clone(),
            stat: self.get_stat().clone(),
        };
        Ok(run)
    }

    fn get_jaccard_index(&self) -> JaccardIndex {
        let v_count = bumpalo::vec![in self.bump; bumpalo::vec![in self.bump; 0usize; self.conf.nbanks()]];
        let mut ji = JaccardIndex::new(self.conf.nbanks());
        let it = v_count.iter();
        ji.count_from_iter(it, cmp_count_wo_singleton);
        // ji.count_from_iter_default(it);
        ji
    }
}


/// A struct to perform sequences collapsing with simple stats: the number of
///  sequences in input banks by collapsed sequences. For this, must store the
///  number of input sequence by bank by collapsed sequence.
///
/// The generic type `<T>` is used for sequences counting.
pub struct HSeqStat<'a, H, T> {
    bump: &'a Bump,
    conf: SeqCollapseConf,
    stat: SeqCollapseStat,
    // hash :
    // * key = sequence as <Vec<u8>>
    // * value = tuple of
    // ** 0: nseq for this seq
    // ** 1: Vec<T> (T: unsigned) : vector of count of this sequence for each bank
    h: HashMap<
        BumpVec<'a, u8>,
        (T,BumpVec<'a, T>),
        BuildHasherDefault<H>,
    >,
}

impl<'a, H, T> HSeqStat<'a, H, T>
where
    T: SeqCount,
    //usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    pub fn new(bump: &'a Bump, conf: SeqCollapseConf) -> HSeqStat<'a, H, T> {
        let nfiles = conf.nbanks();
        HSeqStat {
            bump,
            conf,
            stat: SeqCollapseStat::new(nfiles),
            h: HashMap::<
                BumpVec<u8>,
                (T, BumpVec<T>),
                BuildHasherDefault<H>,
            >::default(),
        }
    }

    pub fn with_capacity(bump: &'a Bump, conf: SeqCollapseConf, capacity: usize) -> HSeqStat<'a, H, T> {
        let nfiles = conf.nbanks();
        HSeqStat {
            bump,
            conf,
            stat: SeqCollapseStat::new(nfiles),
            h: HashMap::<
                BumpVec<u8>,
                (T, BumpVec<T>),
                BuildHasherDefault<H>,
            >::with_capacity(capacity),
        }
    }
}

impl<H, T> SeqCollapse<T> for HSeqStat<'_, H, T>
where
    T: SeqCount,
    //usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    fn get_conf(&self) -> &SeqCollapseConf {
        &self.conf
    }

    fn get_stat(&self) -> &SeqCollapseStat {
        &self.stat
    }

    fn get_mut_stat(&mut self) -> &mut SeqCollapseStat{
        &mut self.stat
    }

    #[allow(unused_variables)]
    fn ins_val(&mut self, seq: Cow<'_,[u8]>, bank_id: BankID, count: T, seq_header: &[u8]) {
        if let Some(t) = self.h.get_mut(seq.deref()) {
            t.0 += count;
            t.1[bank_id as usize] += count;
        } else {
            let mut v = bumpalo::vec![in self.bump; T::ZERO; self.conf.nbanks()];
            v[bank_id as usize] = count;
            let mut new_k = BumpVec::<u8>::with_capacity_in(seq.len(), self.bump);
            unsafe {
                new_k.set_len(seq.len());
            }
            new_k.copy_from_slice(&seq);
            self.h.insert(new_k, (count, v));
        }
    }

    fn get_n_distinct_seq(&self) -> usize {
        self.h.len()
    }

    #[allow(clippy::write_literal)]
    fn out_collapsed_seq(
        &mut self,
        mut out: Out,
        stat_out: Option<Out>,
    ) -> Result<Run, anyhow::Error> {
        let mut stat_out = stat_out.expect("An out file must be specified for statistic output");
        #[cfg(debug)]
        let debug = DEBUG.load(Ordering::Relaxed);
        let factor_norm_freq = self.conf.get_factor_norm_freq();
        writeln!(
            stat_out,
            "{}\t{}\t{}\t{}",
            "new_id", "bank", "nseq", "nseq_norm"
        )?;
        let mut inc_id = IncId::<T>::new();
        let mut n_singleton: usize = 0;
        let mut cl_size_max:T = T::ZERO;
        self.h
            .iter()
            .sorted_unstable_by(|a, b| Ord::cmp(&b.1, &a.1))
            //.enumerate()
            //.for_each(|(iseq, (seq, counts))| {
            .try_for_each(|(seq, counts)| {
                #[cfg(debug)]
                if debug > 1 {
                    eprintln!(
                        "# {} {:?} => {}",
                        iseq,
                        String::from_utf8_lossy(seq),
                        counts.0
                    );
                }
                if counts.0 == T::ONE {
                    n_singleton += 1;
                } else {
                    cl_size_max = cl_size_max.max(counts.0);
                }
                let new_id = inc_id.next_id_with_count(counts.0)?;
                write_fasta(new_id, seq, &mut out, LineEnding::Unix)?;
                for (bank_id, bank_count) in counts.1.iter().enumerate() {
                    if *bank_count == T::ZERO {
                        continue;
                    }
                    writeln!(
                            stat_out,
                            "{}\t{}\t{}\t{:.2}",
                            unsafe { std::str::from_utf8_unchecked(new_id) },
                            self.conf.fastx_tag[bank_id],
                            bank_count,
                            Fundamental::as_f32(*bank_count) / self.stat.v_nseq_tot[bank_id] as f32
                                * factor_norm_freq
                    )?;
                }
                Ok::<(), anyhow::Error>(())
            })?;
        self.stat.n_singleton = n_singleton;
        self.stat.clsize_max = Fundamental::as_usize(cl_size_max);
        let run = Run {
            params: self.get_conf().clone(),
            stat: self.get_stat().clone(),
        };
        Ok(run)
    }

    fn get_jaccard_index(&self) -> JaccardIndex {
        let mut ji = JaccardIndex::new(self.conf.nbanks());
        let it = self.h.values().map(|(_n, v)| v);
        ji.count_from_iter(it, cmp_count_wo_singleton);
        // ji.count_from_iter_default(it);
        ji
    }
}


/// Struct storing counts of sequences from input for a collapsed sequence
#[derive(Debug)]
struct IDBankCount<'a, T> {
    /// The number of sequences in input for this collapsed sequence
    pub nseq: T,
    /// Vector of tuple  (BankID, SeqId, count)
    /// * id of input file (bank)
    /// * id of original sequence
    /// * count of this sequence (in case of option with_add)
    pub v_of_id: BumpVec<'a, (BankID, Vec<u8>, T)>,
}
impl<'a, T> IDBankCount<'a, T>
where
    T: SeqCount,
    //usize: TryFrom<T>,
{
    fn new(bump: &'a Bump, bank_id: BankID, id: &[u8], count: T) -> IDBankCount<'a, T> {
        let mut idbank_count = IDBankCount::<'a, T>::default(bump);
        idbank_count.add(bank_id, id, count);
        idbank_count
    }

    fn default(bump: &'a Bump) -> IDBankCount<'a, T> {
        IDBankCount {
            nseq: T::ZERO,
            v_of_id: BumpVec::new_in(bump),
        }
    }

    fn add(&mut self, bank_id: BankID, id: &[u8], count: T) {
        self.nseq += count;
        self.v_of_id.push((bank_id, id.to_vec(), count));
    }
}


/// A struct to perform sequence collapsing with complete stats. For this, must
///  store the all sequences id corresponding by collapsed sequence.
///
/// The generic type `<T>` is used for sequences counting.
pub struct HSeqStat2<'a, H, T> {
    bump: &'a Bump,
    conf: SeqCollapseConf,
    stat: SeqCollapseStat,
    // hash :
    // * key = sequence as <Vec<u8>>
    // * value = Vec of IDBankCount
    h: HashMap<
        BumpVec<'a, u8>,
        IDBankCount<'a, T>,
        BuildHasherDefault<H>,
    >,
}

impl<'a, H, T> HSeqStat2<'a, H, T>
where
    T: SeqCount,
    // usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    pub fn new(bump: &'a Bump, conf: SeqCollapseConf) -> HSeqStat2<'a, H, T> {
        let nfiles = conf.nbanks();
        HSeqStat2 {
            bump,
            conf,
            stat: SeqCollapseStat::new(nfiles),
            h: HashMap::<
                BumpVec<'a, u8>,
                IDBankCount<'a, T>,
                BuildHasherDefault<H>,
            >::default(),
        }
    }

    pub fn with_capacity(
        bump: &'a Bump,
        conf: SeqCollapseConf,
        capacity: usize,
    ) -> HSeqStat2<'a, H, T> {
        let nfiles = conf.nbanks();
        HSeqStat2 {
            bump,
            conf,
            stat: SeqCollapseStat::new(nfiles),
            h: HashMap::<
                BumpVec<'a, u8>,
                IDBankCount<'a, T>,
                BuildHasherDefault<H>,
            >::with_capacity(capacity),
        }
    }
}

impl<H, T> SeqCollapse<T> for HSeqStat2<'_, H, T>
where
    T: SeqCount,
    // usize: TryFrom<T>,
    H: Hasher,
    BuildHasherDefault<H>: BuildHasher,
{
    fn get_conf(&self) -> &SeqCollapseConf {
        &self.conf
    }

    fn get_stat(&self) -> &SeqCollapseStat {
        &self.stat
    }

    fn get_mut_stat(&mut self) -> &mut SeqCollapseStat {
        &mut self.stat
    }

    fn ins_val(&mut self, seq: Cow<'_,[u8]>, bank_id: BankID, count: T, seq_header: &[u8]){
        let id = get_seq_id(seq_header);
        if let Some(v) = self.h.get_mut(seq.deref()) {
            v.add(bank_id, id, count);
        } else {
            let mut new_k = BumpVec::<u8>::with_capacity_in(seq.len(), self.bump);
            unsafe {
                new_k.set_len(seq.len());
            }
            new_k.copy_from_slice(&seq);
            self.h.insert(
                new_k,
                IDBankCount::new(self.bump, bank_id, id, count),
            );
        }
    }

    fn get_n_distinct_seq(&self) -> usize {
        self.h.len()
    }

    #[allow(clippy::write_literal)]
    fn out_collapsed_seq(
        &mut self,
        mut out: Out,
        stat_out: Option<Out>,
    ) -> Result<Run, anyhow::Error> {
        let mut stat_out = stat_out.expect("An out file must be specified for statistic output");
        #[cfg(debug)]
        let debug = DEBUG.load(Ordering::Relaxed);
        let factor_norm_freq = self.conf.get_factor_norm_freq();
        writeln!(
            stat_out,
            "{}\t{}\t{}\t{}\t{}",
            "new_id", "bank", "old_id", "nseq", "nseq_norm"
        )?;
        let mut inc_id = IncId::<T>::new();
        let mut n_singleton = 0;
        let mut clsize_max = T::ZERO;
        self.h
            .iter()
            .sorted_unstable_by(|a, b| Ord::cmp(&b.1.nseq, &a.1.nseq))
            .try_for_each(|(seq, v_of_idbankcount)| {
                let count = v_of_idbankcount.nseq;
                #[cfg(debug)]
                if debug > 1 {
                    eprintln!("# {} {:?} => {}", iseq, String::from_utf8_lossy(seq), nseq);
                }
                if count == T::ONE {
                    n_singleton += 1
                } else {
                    clsize_max = clsize_max.max(count);
                }
                let new_id = inc_id.next_id_with_count(count)?;
                write_fasta(new_id, seq, &mut out, LineEnding::Unix)?;
                for (bank_id, id, count) in v_of_idbankcount.v_of_id.iter() {
                    writeln!(
                        stat_out,
                        "{}\t{}\t{}\t{}\t{}",
                        unsafe { std::str::from_utf8_unchecked(new_id) },
                        self.conf.fastx_tag[*bank_id as usize],
                        unsafe { std::str::from_utf8_unchecked(id) },
                        count,
                        Fundamental::as_f32(*count) / self.stat.v_nseq_tot[*bank_id as usize] as f32
                            * factor_norm_freq
                    )?;
                }
                Ok::<(), anyhow::Error>(())
            })?;
        self.stat.n_singleton = n_singleton;
        self.stat.clsize_max = Fundamental::as_usize(clsize_max);
        let run = Run {
            params: self.get_conf().clone(),
            stat: self.get_stat().clone(),
        };
        Ok(run)
    }

    fn get_jaccard_index(&self) -> JaccardIndex {
        let nbanks = self.conf.nbanks();
        let mut ji = JaccardIndex::new(nbanks);
        //let mut v_count = bumpalo::vec![in self.bump; T::ZERO; nbanks];
        let it = self.h.values()
            .map(|idbankcount| {
                // v_count.iter_mut().for_each(|v| *v = T::ZERO);
                let mut v_count = bumpalo::vec![in self.bump; T::ZERO; nbanks];
                idbankcount
                    .v_of_id
                    .iter()
                    .for_each(|(ibank, _idseq, count)| {
                        // eprint!("({}) i{} v{} -- ", unsafe { std::str::from_utf8_unchecked(_idseq)} ,ibank, count);
                        v_count[*ibank as usize] += *count
                    });
                // eprintln!("");
                // print_v_count(&v_count);
                v_count
            });
        // ji.count_from_iter(it, cmp_count);
        ji.count_from_iter(it, cmp_count_wo_singleton);
        ji
    }
}
