use std::{error::Error, io::Read, path::Path};

use bumpalo::Bump;
use lazy_static::lazy_static;

use ::seqcollapse::seqcollapse::HSeqCount;
use criterion::{
    criterion_group, criterion_main, measurement::WallTime, BenchmarkGroup, Criterion,
};
use flate2::bufread::GzDecoder;
use seqcollapse::seqcollapseconf::SeqCollapseConf;
use slurp::read_all_bytes;



lazy_static! {
    static ref DATA_FQ: Vec<u8> = gzread_file("data/test1k.fastq.gz").unwrap();
    static ref DATA_PEP: Vec<u8> = gzread_file("data/pep1k.fa.gz").unwrap();
    static ref SEQCOLLAPSECONF: SeqCollapseConf = SeqCollapseConf::_new_for_input_buffer();
}

fn gzread_file<P: AsRef<Path>>(filename: P) -> Result<Vec<u8>, Box<dyn Error>> {
    // eprintln!("gzread_file");
    let data = read_all_bytes(filename)?;
    gzread_data(&data)
}

fn gzread_data(data: &[u8]) -> Result<Vec<u8>, Box<dyn Error>> {
    let mut gz = GzDecoder::new(&data[..]);
    let mut uncompressed: Vec<u8> = vec![];
    let _res = gz.read_to_end(&mut uncompressed)?;
    Ok(uncompressed)
}

#[allow(non_snake_case)]
fn hasher_AHasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<ahash::AHasher, u64> = HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_fnv(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fnv::FnvHasher, u64> = HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_foldhasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<seqcollapse::FoldHasher, u64> = HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_FxHasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fxhash::FxHasher, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_FxHasher_rustc(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<rustc_hash::FxHasher, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_FxHasher64(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fxhash::FxHasher64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_AvxHighwayHasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<highway::AvxHash, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_SseHighwayHasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<highway::SseHash, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
pub fn hasher_PortableHighwayHasher(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<highway::PortableHash, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_metro1(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::metro::crc::Hasher64_1, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_metro2(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::metro::crc::Hasher64_2, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_murmur3_x64(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::murmur3::Hasher128_x64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_murmur3_x86(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::murmur3::Hasher128_x86, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_sea(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::sea::Hasher64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_siphash13(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<siphasher::sip::SipHasher13, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_siphash24(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<siphasher::sip::SipHasher24, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_t1ha0(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::t1ha0::Hasher64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_t1ha1(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<t1ha::T1haHasher, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_t1ha2(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<fasthash::t1ha2::Hasher64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn hasher_wyhash(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<wyhash::WyHash, u64> = HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
fn hasher_XxHash64(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<twox_hash::XxHash64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

#[allow(non_snake_case)]
fn hasher_Xxh3Hash64(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<twox_hash::XxHash3_64, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

fn hasher_std(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<std::collections::hash_map::DefaultHasher, u64> =
        HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

type FnBenchHasher = for<'a> fn(&'a Bump, &'static [u8]);

pub fn bench_hasher<'b>(
    group: &'b mut BenchmarkGroup<WallTime>,
    mut bump: &'b mut Bump,
    buff: &'static [u8],
    id: &'b str,
    f: FnBenchHasher,
) {
    group.bench_function(id, |b| {
        b.iter(|| {
            f(&mut bump, buff);
            bump.reset();
        })
    });
}

pub fn bench_hashers(mut group: BenchmarkGroup<WallTime>, bump: &mut Bump, buff: &'static [u8]) {
    let v_of_h: Vec<(&str, FnBenchHasher)> = vec![
        (stringify!(hasher_std), hasher_std),
        (stringify!(hasher_AHasher), hasher_AHasher),
        (stringify!(hasher_fnv), hasher_fnv),
        (stringify!(hasher_foldhasher), hasher_foldhasher),
        (stringify!(hasher_FxHasher), hasher_FxHasher),
        (stringify!(hasher_FxHasher64), hasher_FxHasher64),
        (stringify!(hasher_FxHasher_rustc), hasher_FxHasher_rustc),
        (stringify!(hasher_AvxHighwayHasher), hasher_AvxHighwayHasher),
        (stringify!(hasher_SseHighwayHasher), hasher_SseHighwayHasher),
        (
            stringify!(hasher_PortableHighwayHasher),
            hasher_PortableHighwayHasher,
        ),
        (stringify!(hasher_metro1), hasher_metro1),
        (stringify!(hasher_metro2), hasher_metro2),
        (stringify!(hasher_murmur3_x64), hasher_murmur3_x64),
        (stringify!(hasher_murmur3_x86), hasher_murmur3_x86),
        (stringify!(hasher_sea), hasher_sea),
        (stringify!(hasher_siphash13), hasher_siphash13),
        (stringify!(hasher_siphash24), hasher_siphash24),
        (stringify!(hasher_t1ha0), hasher_t1ha0),
        (stringify!(hasher_t1ha1), hasher_t1ha1),
        (stringify!(hasher_t1ha2), hasher_t1ha2),
        (stringify!(hasher_wyhash), hasher_wyhash),
        (stringify!(hasher_XxHash64), hasher_XxHash64),
        (stringify!(hasher_Xxh3Hash64), hasher_Xxh3Hash64),
    ];
    for (id, func) in v_of_h {
        bench_hasher(&mut group, bump, buff, id, func);
        // eprintln!("# bump size={}", bump.allocated_bytes());
    }
    group.finish()
}

pub fn fq(c: &mut Criterion) {
    let mut bump = Bump::new();
    let buff: &'static [u8] = &DATA_FQ;
    let group = c.benchmark_group("fq");
    let vbytes: Vec<u8> = vec![1, 2, 3, 4, 5, 6, 7, 8];
    let bytes = &vbytes[..8];
    let read_usize = |bytes: &[u8]| u64::from_ne_bytes(bytes[..8].try_into().unwrap());
    let _res = read_usize(bytes);
    bench_hashers(group, &mut bump, buff)
}

pub fn pep(c: &mut Criterion) {
    let mut bump = Bump::new();
    let buff: &'static [u8] = &DATA_PEP;
    let group = c.benchmark_group("pep");
    bench_hashers(group, &mut bump, buff)
}

#[allow(non_snake_case)]
fn hasher_AHasher_u32(bump: &Bump, buff: &'static [u8]) {
    let mut hseq: HSeqCount<ahash::AHasher, u64> = HSeqCount::new(bump, SeqCollapseConf::_new_for_input_buffer());
    let _res = hseq.collapse_seq_from_buff(buff);
}

pub fn fq_key_size(c: &mut Criterion) {
    let mut bump = Bump::new();
    let buff: &'static [u8] = &DATA_FQ;
    let mut group = c.benchmark_group("fq_key_size");

    group.bench_function("u64", |b| b.iter(|| hasher_AHasher(&bump, buff)));
    bump.reset();
    group.bench_function("u32", |b| b.iter(|| hasher_AHasher_u32(&bump, buff)));
    bump.reset();
    group.finish()
}

pub fn pep_key_size(c: &mut Criterion) {
    let mut bump = Bump::new();
    let buff: &'static [u8] = &DATA_PEP;
    let mut group = c.benchmark_group("pep_key_size");

    group.bench_function("u64", |b| b.iter(|| hasher_AHasher(&bump, buff)));
    bump.reset();
    group.bench_function("u32", |b| b.iter(|| hasher_AHasher_u32(&bump, buff)));
    bump.reset();
    group.finish()
}

criterion_group! {
    name = benches;
    config = Criterion::default().sample_size(10);
    targets = fq, pep, fq_key_size, pep_key_size
}

criterion_main!(benches);
